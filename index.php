/* Code tidied up by ScrapBook */
.ds-form.dsapplication .fl-block { margin-right: 5px; }
.ds-form.dsapplication .services .fl-block { margin-right: 25px; }
.ds-form.dsapplication .services .field.file { margin-top: 20px; }
.ds-form.dsapplication .data .fields .field { width: 50%; margin-bottom: 15px ! important; }
.ds-form.dsapplication .data .fields .field input { width: 100%; }
@media (max-width: 1025px) {
}
@media (max-width: 750px) {
  .ds-form.dsapplication .data .fields .field { width: 100%; }
}
